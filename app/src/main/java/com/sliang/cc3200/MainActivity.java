package com.sliang.cc3200;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.view.View;

public class MainActivity extends SingleFragmentActivity {
    @Override
    public Fragment createFragment() {
        return new JoyStickFragment();
    }
}
