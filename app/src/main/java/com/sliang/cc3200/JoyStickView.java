package com.sliang.cc3200;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;

/**
 * Created by sliang on 6/3/2015.
 */
public class JoyStickView extends View {
    public static final String TAG = "JoyStickView";

    private Paint mBoxPaint;
    private Paint mPadPaint;
    private Paint p_firstTouchColor;
    private Paint p_secondTouchColor;
    private Paint mBackgroundPaint;
    private int vwidth;
    private int vheight;
    private float stick_rad = 100;
    private int pad_rad = 250;
    PadArea PadLeft;
    PadArea PadRight;
    View mView;
    UDPSender UDPSenderThread;
    int sb_h = 240;
    int sb_w = 240;
    BitmapFactory.Options options = new BitmapFactory.Options();
    Bitmap b = BitmapFactory.decodeResource(getResources(), R.mipmap.black_cross_button, options);
    Bitmap sb = Bitmap.createScaledBitmap(b, sb_h, sb_w, false);


    public JoyStickView(Context context) {
        this(context, null);
    }

    public JoyStickView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mBoxPaint = new Paint();
        mBoxPaint.setColor(Color.BLACK);

        mPadPaint = new Paint();
        mPadPaint.setColor(Color.DKGRAY);

        this.setBackgroundColor(Color.BLACK);

        p_firstTouchColor = new Paint();
        p_firstTouchColor.setColor(Color.LTGRAY);

        p_secondTouchColor = new Paint();
        p_secondTouchColor.setColor(Color.RED);

        mView = this;

        this.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Log.i(TAG, "onGlobalLayout()");
                vheight = mView.getHeight();
                vwidth = mView.getWidth();
                Log.i(TAG, "h = " + Integer.toString(vheight) + " w = " + Integer.toString(vwidth));

                PadLeft = new PadArea(pad_rad, vheight - pad_rad, pad_rad);

                PadRight = new PadArea(vwidth - pad_rad, vheight - pad_rad, pad_rad);
            }
        });

        UDPSenderThread = new UDPSender("UDPSenderThread");
        UDPSenderThread.start();
    }

    public boolean onTouchEvent(MotionEvent event) {
        int idx = event.getActionIndex();

        switch(MotionEventCompat.getActionMasked(event)) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:
                Log.i(TAG, "ACTION_DOWN actionIndex = " + Integer.toString(idx));
                PadRight.trySet((int) event.getX(idx), (int) event.getY(idx), idx);
                PadLeft.trySet((int) event.getX(idx), (int) event.getY(idx), idx);
                break;
            case MotionEvent.ACTION_UP:
                PadRight.forceClear();
                PadLeft.forceClear();
                break;
            case MotionEvent.ACTION_POINTER_UP:
                Log.i(TAG, "ACTION_UP actionIndex = " + Integer.toString(idx));
                PadRight.tryClear(idx);
                PadLeft.tryClear(idx);
                break;
            case MotionEvent.ACTION_MOVE:
                int cnt = event.getPointerCount();
                for (int i = 0; i < cnt && i < 2; i++) {
                    PadRight.tryMove((int) event.getX(i), (int) event.getY(i), i);
                    PadLeft.tryMove((int) event.getX(i), (int) event.getY(i), i);
                }
                break;
            default:
                Log.i(TAG, "UNKNOWN, action_id = " + Integer.toString(event.getAction()));
                break;
        }

        invalidate();
        return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // draw background pads that change with color
        if (PadLeft.getId() >= 0)
            canvas.drawCircle(PadLeft.getCenterX(), PadLeft.getCenterY(), pad_rad, p_firstTouchColor);
        else
            canvas.drawCircle(PadLeft.getCenterX(), PadLeft.getCenterY(), pad_rad, mPadPaint);

        if (PadRight.getId() >= 0)
            canvas.drawCircle(PadRight.getCenterX(), PadRight.getCenterY(), pad_rad, p_firstTouchColor);
        else
            canvas.drawCircle(PadRight.getCenterX(), PadRight.getCenterY(), pad_rad, mPadPaint);

        // draw foreground pads using bitmap image
        canvas.drawBitmap(sb, PadLeft.getX() - sb_h/2, PadLeft.getY() - sb_w/2, mBoxPaint);
        canvas.drawBitmap(sb, PadRight.getX() - sb_h/2, PadRight.getY() - sb_w/2, mBoxPaint);

        UDPSenderThread.send(PadLeft.getX() - PadLeft.getCenterX(), // XLeft
                PadRight.getX() - PadRight.getCenterX(), // XRight
                PadLeft.getCenterY() - PadLeft.getY(), // YLeft
                PadRight.getCenterY() - PadRight.getY(), // YRight
                0);
    }
}