package com.sliang.cc3200;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by sliang on 6/3/2015.
 */
public class JoyStickFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_joystick, parent, false);
        v.setBackgroundColor(Color.BLACK);
        return v;
    }
}
