package com.sliang.cc3200;

import android.util.Log;

/**
 * Created by sliang on 6/3/2015.
 */
public class PadArea {
    private String TAG = "PadArea";
    int mCenterX;
    int mCenterY;
    int mX;
    int mY;
    int mRadius;
    int mRadiusAdjust = 50;
    int mIndex; // pointer index

    public PadArea(int x, int y, int r) {
        mCenterX = x;
        mX = x;
        mCenterY = y;
        mY = y;
        mRadius = r;
        mIndex = -1;
    }

    public int getCenterX() {
        return mCenterX;
    }

    public int getCenterY() {
        return mCenterY;
    }

    public int getX() {
        return mX;
    }

    public int getY() {
        return mY;
    }

    public int getId() {
        return mIndex;
    }

    public void tryMove(int x, int y, int id) {
        if (mIndex == id) {
            if (this.contains(x, y)) {
                mX = x;
                mY = y;
            } else {
                mX = mCenterX;
                mY = mCenterY;
            }
        }
    }

    public boolean contains(int x, int y) {
        double dxx = (int) Math.pow(x - mCenterX, 2);
        double dyy = (int) Math.pow(y - mCenterY, 2);
        return (Math.sqrt(dxx + dyy) <= (mRadius));
    }

    public boolean trySet(int x, int y, int id) {
        if (this.contains(x, y) && mIndex < 0) {
            mX = x;
            mY = y;
            mIndex = id;
            return true;
        }
        return false;
    }

    public void forceClear() {
        mIndex = -1;
        mX = mCenterX;
        mY = mCenterY;
    }

    public boolean tryClear(int id) {
        if (mIndex == id) {
            mIndex = -1;
            mX = mCenterX;
            mY = mCenterY;
            return true;
        }
        return false;
    }
}
