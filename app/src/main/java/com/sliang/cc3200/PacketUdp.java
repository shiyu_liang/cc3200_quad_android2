// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: udp_cmd.proto

package com.sliang.cc3200;

public final class PacketUdp {
  private PacketUdp() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
  }
  public interface UdpCmdOrBuilder extends
      // @@protoc_insertion_point(interface_extends:com.sliang.cc3200.UdpCmd)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <code>required sfixed32 x_Left = 1;</code>
     */
    boolean hasXLeft();
    /**
     * <code>required sfixed32 x_Left = 1;</code>
     */
    int getXLeft();

    /**
     * <code>required sfixed32 x_Right = 2;</code>
     */
    boolean hasXRight();
    /**
     * <code>required sfixed32 x_Right = 2;</code>
     */
    int getXRight();

    /**
     * <code>required sfixed32 y_Left = 3;</code>
     */
    boolean hasYLeft();
    /**
     * <code>required sfixed32 y_Left = 3;</code>
     */
    int getYLeft();

    /**
     * <code>required sfixed32 y_Right = 4;</code>
     */
    boolean hasYRight();
    /**
     * <code>required sfixed32 y_Right = 4;</code>
     */
    int getYRight();

    /**
     * <code>required sfixed32 timestamp = 5;</code>
     */
    boolean hasTimestamp();
    /**
     * <code>required sfixed32 timestamp = 5;</code>
     */
    int getTimestamp();
  }
  /**
   * Protobuf type {@code com.sliang.cc3200.UdpCmd}
   */
  public static final class UdpCmd extends
      com.google.protobuf.GeneratedMessage implements
      // @@protoc_insertion_point(message_implements:com.sliang.cc3200.UdpCmd)
      UdpCmdOrBuilder {
    // Use UdpCmd.newBuilder() to construct.
    private UdpCmd(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
      super(builder);
      this.unknownFields = builder.getUnknownFields();
    }
    private UdpCmd(boolean noInit) { this.unknownFields = com.google.protobuf.UnknownFieldSet.getDefaultInstance(); }

    private static final UdpCmd defaultInstance;
    public static UdpCmd getDefaultInstance() {
      return defaultInstance;
    }

    public UdpCmd getDefaultInstanceForType() {
      return defaultInstance;
    }

    private final com.google.protobuf.UnknownFieldSet unknownFields;
    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
        getUnknownFields() {
      return this.unknownFields;
    }
    private UdpCmd(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      initFields();
      int mutable_bitField0_ = 0;
      com.google.protobuf.UnknownFieldSet.Builder unknownFields =
          com.google.protobuf.UnknownFieldSet.newBuilder();
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            default: {
              if (!parseUnknownField(input, unknownFields,
                                     extensionRegistry, tag)) {
                done = true;
              }
              break;
            }
            case 13: {
              bitField0_ |= 0x00000001;
              xLeft_ = input.readSFixed32();
              break;
            }
            case 21: {
              bitField0_ |= 0x00000002;
              xRight_ = input.readSFixed32();
              break;
            }
            case 29: {
              bitField0_ |= 0x00000004;
              yLeft_ = input.readSFixed32();
              break;
            }
            case 37: {
              bitField0_ |= 0x00000008;
              yRight_ = input.readSFixed32();
              break;
            }
            case 45: {
              bitField0_ |= 0x00000010;
              timestamp_ = input.readSFixed32();
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e.getMessage()).setUnfinishedMessage(this);
      } finally {
        this.unknownFields = unknownFields.build();
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.sliang.cc3200.PacketUdp.internal_static_com_sliang_cc3200_UdpCmd_descriptor;
    }

    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.sliang.cc3200.PacketUdp.internal_static_com_sliang_cc3200_UdpCmd_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.sliang.cc3200.PacketUdp.UdpCmd.class, com.sliang.cc3200.PacketUdp.UdpCmd.Builder.class);
    }

    public static com.google.protobuf.Parser<UdpCmd> PARSER =
        new com.google.protobuf.AbstractParser<UdpCmd>() {
      public UdpCmd parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
        return new UdpCmd(input, extensionRegistry);
      }
    };

    @java.lang.Override
    public com.google.protobuf.Parser<UdpCmd> getParserForType() {
      return PARSER;
    }

    private int bitField0_;
    public static final int X_LEFT_FIELD_NUMBER = 1;
    private int xLeft_;
    /**
     * <code>required sfixed32 x_Left = 1;</code>
     */
    public boolean hasXLeft() {
      return ((bitField0_ & 0x00000001) == 0x00000001);
    }
    /**
     * <code>required sfixed32 x_Left = 1;</code>
     */
    public int getXLeft() {
      return xLeft_;
    }

    public static final int X_RIGHT_FIELD_NUMBER = 2;
    private int xRight_;
    /**
     * <code>required sfixed32 x_Right = 2;</code>
     */
    public boolean hasXRight() {
      return ((bitField0_ & 0x00000002) == 0x00000002);
    }
    /**
     * <code>required sfixed32 x_Right = 2;</code>
     */
    public int getXRight() {
      return xRight_;
    }

    public static final int Y_LEFT_FIELD_NUMBER = 3;
    private int yLeft_;
    /**
     * <code>required sfixed32 y_Left = 3;</code>
     */
    public boolean hasYLeft() {
      return ((bitField0_ & 0x00000004) == 0x00000004);
    }
    /**
     * <code>required sfixed32 y_Left = 3;</code>
     */
    public int getYLeft() {
      return yLeft_;
    }

    public static final int Y_RIGHT_FIELD_NUMBER = 4;
    private int yRight_;
    /**
     * <code>required sfixed32 y_Right = 4;</code>
     */
    public boolean hasYRight() {
      return ((bitField0_ & 0x00000008) == 0x00000008);
    }
    /**
     * <code>required sfixed32 y_Right = 4;</code>
     */
    public int getYRight() {
      return yRight_;
    }

    public static final int TIMESTAMP_FIELD_NUMBER = 5;
    private int timestamp_;
    /**
     * <code>required sfixed32 timestamp = 5;</code>
     */
    public boolean hasTimestamp() {
      return ((bitField0_ & 0x00000010) == 0x00000010);
    }
    /**
     * <code>required sfixed32 timestamp = 5;</code>
     */
    public int getTimestamp() {
      return timestamp_;
    }

    private void initFields() {
      xLeft_ = 0;
      xRight_ = 0;
      yLeft_ = 0;
      yRight_ = 0;
      timestamp_ = 0;
    }
    private byte memoizedIsInitialized = -1;
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      if (!hasXLeft()) {
        memoizedIsInitialized = 0;
        return false;
      }
      if (!hasXRight()) {
        memoizedIsInitialized = 0;
        return false;
      }
      if (!hasYLeft()) {
        memoizedIsInitialized = 0;
        return false;
      }
      if (!hasYRight()) {
        memoizedIsInitialized = 0;
        return false;
      }
      if (!hasTimestamp()) {
        memoizedIsInitialized = 0;
        return false;
      }
      memoizedIsInitialized = 1;
      return true;
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      getSerializedSize();
      if (((bitField0_ & 0x00000001) == 0x00000001)) {
        output.writeSFixed32(1, xLeft_);
      }
      if (((bitField0_ & 0x00000002) == 0x00000002)) {
        output.writeSFixed32(2, xRight_);
      }
      if (((bitField0_ & 0x00000004) == 0x00000004)) {
        output.writeSFixed32(3, yLeft_);
      }
      if (((bitField0_ & 0x00000008) == 0x00000008)) {
        output.writeSFixed32(4, yRight_);
      }
      if (((bitField0_ & 0x00000010) == 0x00000010)) {
        output.writeSFixed32(5, timestamp_);
      }
      getUnknownFields().writeTo(output);
    }

    private int memoizedSerializedSize = -1;
    public int getSerializedSize() {
      int size = memoizedSerializedSize;
      if (size != -1) return size;

      size = 0;
      if (((bitField0_ & 0x00000001) == 0x00000001)) {
        size += com.google.protobuf.CodedOutputStream
          .computeSFixed32Size(1, xLeft_);
      }
      if (((bitField0_ & 0x00000002) == 0x00000002)) {
        size += com.google.protobuf.CodedOutputStream
          .computeSFixed32Size(2, xRight_);
      }
      if (((bitField0_ & 0x00000004) == 0x00000004)) {
        size += com.google.protobuf.CodedOutputStream
          .computeSFixed32Size(3, yLeft_);
      }
      if (((bitField0_ & 0x00000008) == 0x00000008)) {
        size += com.google.protobuf.CodedOutputStream
          .computeSFixed32Size(4, yRight_);
      }
      if (((bitField0_ & 0x00000010) == 0x00000010)) {
        size += com.google.protobuf.CodedOutputStream
          .computeSFixed32Size(5, timestamp_);
      }
      size += getUnknownFields().getSerializedSize();
      memoizedSerializedSize = size;
      return size;
    }

    private static final long serialVersionUID = 0L;
    @java.lang.Override
    protected java.lang.Object writeReplace()
        throws java.io.ObjectStreamException {
      return super.writeReplace();
    }

    public static com.sliang.cc3200.PacketUdp.UdpCmd parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static com.sliang.cc3200.PacketUdp.UdpCmd parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static com.sliang.cc3200.PacketUdp.UdpCmd parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static com.sliang.cc3200.PacketUdp.UdpCmd parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static com.sliang.cc3200.PacketUdp.UdpCmd parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return PARSER.parseFrom(input);
    }
    public static com.sliang.cc3200.PacketUdp.UdpCmd parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return PARSER.parseFrom(input, extensionRegistry);
    }
    public static com.sliang.cc3200.PacketUdp.UdpCmd parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return PARSER.parseDelimitedFrom(input);
    }
    public static com.sliang.cc3200.PacketUdp.UdpCmd parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return PARSER.parseDelimitedFrom(input, extensionRegistry);
    }
    public static com.sliang.cc3200.PacketUdp.UdpCmd parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return PARSER.parseFrom(input);
    }
    public static com.sliang.cc3200.PacketUdp.UdpCmd parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return PARSER.parseFrom(input, extensionRegistry);
    }

    public static Builder newBuilder() { return Builder.create(); }
    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder(com.sliang.cc3200.PacketUdp.UdpCmd prototype) {
      return newBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() { return newBuilder(this); }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code com.sliang.cc3200.UdpCmd}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessage.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:com.sliang.cc3200.UdpCmd)
        com.sliang.cc3200.PacketUdp.UdpCmdOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return com.sliang.cc3200.PacketUdp.internal_static_com_sliang_cc3200_UdpCmd_descriptor;
      }

      protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return com.sliang.cc3200.PacketUdp.internal_static_com_sliang_cc3200_UdpCmd_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                com.sliang.cc3200.PacketUdp.UdpCmd.class, com.sliang.cc3200.PacketUdp.UdpCmd.Builder.class);
      }

      // Construct using com.sliang.cc3200.PacketUdp.UdpCmd.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessage.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
        }
      }
      private static Builder create() {
        return new Builder();
      }

      public Builder clear() {
        super.clear();
        xLeft_ = 0;
        bitField0_ = (bitField0_ & ~0x00000001);
        xRight_ = 0;
        bitField0_ = (bitField0_ & ~0x00000002);
        yLeft_ = 0;
        bitField0_ = (bitField0_ & ~0x00000004);
        yRight_ = 0;
        bitField0_ = (bitField0_ & ~0x00000008);
        timestamp_ = 0;
        bitField0_ = (bitField0_ & ~0x00000010);
        return this;
      }

      public Builder clone() {
        return create().mergeFrom(buildPartial());
      }

      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return com.sliang.cc3200.PacketUdp.internal_static_com_sliang_cc3200_UdpCmd_descriptor;
      }

      public com.sliang.cc3200.PacketUdp.UdpCmd getDefaultInstanceForType() {
        return com.sliang.cc3200.PacketUdp.UdpCmd.getDefaultInstance();
      }

      public com.sliang.cc3200.PacketUdp.UdpCmd build() {
        com.sliang.cc3200.PacketUdp.UdpCmd result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      public com.sliang.cc3200.PacketUdp.UdpCmd buildPartial() {
        com.sliang.cc3200.PacketUdp.UdpCmd result = new com.sliang.cc3200.PacketUdp.UdpCmd(this);
        int from_bitField0_ = bitField0_;
        int to_bitField0_ = 0;
        if (((from_bitField0_ & 0x00000001) == 0x00000001)) {
          to_bitField0_ |= 0x00000001;
        }
        result.xLeft_ = xLeft_;
        if (((from_bitField0_ & 0x00000002) == 0x00000002)) {
          to_bitField0_ |= 0x00000002;
        }
        result.xRight_ = xRight_;
        if (((from_bitField0_ & 0x00000004) == 0x00000004)) {
          to_bitField0_ |= 0x00000004;
        }
        result.yLeft_ = yLeft_;
        if (((from_bitField0_ & 0x00000008) == 0x00000008)) {
          to_bitField0_ |= 0x00000008;
        }
        result.yRight_ = yRight_;
        if (((from_bitField0_ & 0x00000010) == 0x00000010)) {
          to_bitField0_ |= 0x00000010;
        }
        result.timestamp_ = timestamp_;
        result.bitField0_ = to_bitField0_;
        onBuilt();
        return result;
      }

      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof com.sliang.cc3200.PacketUdp.UdpCmd) {
          return mergeFrom((com.sliang.cc3200.PacketUdp.UdpCmd)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(com.sliang.cc3200.PacketUdp.UdpCmd other) {
        if (other == com.sliang.cc3200.PacketUdp.UdpCmd.getDefaultInstance()) return this;
        if (other.hasXLeft()) {
          setXLeft(other.getXLeft());
        }
        if (other.hasXRight()) {
          setXRight(other.getXRight());
        }
        if (other.hasYLeft()) {
          setYLeft(other.getYLeft());
        }
        if (other.hasYRight()) {
          setYRight(other.getYRight());
        }
        if (other.hasTimestamp()) {
          setTimestamp(other.getTimestamp());
        }
        this.mergeUnknownFields(other.getUnknownFields());
        return this;
      }

      public final boolean isInitialized() {
        if (!hasXLeft()) {
          
          return false;
        }
        if (!hasXRight()) {
          
          return false;
        }
        if (!hasYLeft()) {
          
          return false;
        }
        if (!hasYRight()) {
          
          return false;
        }
        if (!hasTimestamp()) {
          
          return false;
        }
        return true;
      }

      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        com.sliang.cc3200.PacketUdp.UdpCmd parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (com.sliang.cc3200.PacketUdp.UdpCmd) e.getUnfinishedMessage();
          throw e;
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }
      private int bitField0_;

      private int xLeft_ ;
      /**
       * <code>required sfixed32 x_Left = 1;</code>
       */
      public boolean hasXLeft() {
        return ((bitField0_ & 0x00000001) == 0x00000001);
      }
      /**
       * <code>required sfixed32 x_Left = 1;</code>
       */
      public int getXLeft() {
        return xLeft_;
      }
      /**
       * <code>required sfixed32 x_Left = 1;</code>
       */
      public Builder setXLeft(int value) {
        bitField0_ |= 0x00000001;
        xLeft_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>required sfixed32 x_Left = 1;</code>
       */
      public Builder clearXLeft() {
        bitField0_ = (bitField0_ & ~0x00000001);
        xLeft_ = 0;
        onChanged();
        return this;
      }

      private int xRight_ ;
      /**
       * <code>required sfixed32 x_Right = 2;</code>
       */
      public boolean hasXRight() {
        return ((bitField0_ & 0x00000002) == 0x00000002);
      }
      /**
       * <code>required sfixed32 x_Right = 2;</code>
       */
      public int getXRight() {
        return xRight_;
      }
      /**
       * <code>required sfixed32 x_Right = 2;</code>
       */
      public Builder setXRight(int value) {
        bitField0_ |= 0x00000002;
        xRight_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>required sfixed32 x_Right = 2;</code>
       */
      public Builder clearXRight() {
        bitField0_ = (bitField0_ & ~0x00000002);
        xRight_ = 0;
        onChanged();
        return this;
      }

      private int yLeft_ ;
      /**
       * <code>required sfixed32 y_Left = 3;</code>
       */
      public boolean hasYLeft() {
        return ((bitField0_ & 0x00000004) == 0x00000004);
      }
      /**
       * <code>required sfixed32 y_Left = 3;</code>
       */
      public int getYLeft() {
        return yLeft_;
      }
      /**
       * <code>required sfixed32 y_Left = 3;</code>
       */
      public Builder setYLeft(int value) {
        bitField0_ |= 0x00000004;
        yLeft_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>required sfixed32 y_Left = 3;</code>
       */
      public Builder clearYLeft() {
        bitField0_ = (bitField0_ & ~0x00000004);
        yLeft_ = 0;
        onChanged();
        return this;
      }

      private int yRight_ ;
      /**
       * <code>required sfixed32 y_Right = 4;</code>
       */
      public boolean hasYRight() {
        return ((bitField0_ & 0x00000008) == 0x00000008);
      }
      /**
       * <code>required sfixed32 y_Right = 4;</code>
       */
      public int getYRight() {
        return yRight_;
      }
      /**
       * <code>required sfixed32 y_Right = 4;</code>
       */
      public Builder setYRight(int value) {
        bitField0_ |= 0x00000008;
        yRight_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>required sfixed32 y_Right = 4;</code>
       */
      public Builder clearYRight() {
        bitField0_ = (bitField0_ & ~0x00000008);
        yRight_ = 0;
        onChanged();
        return this;
      }

      private int timestamp_ ;
      /**
       * <code>required sfixed32 timestamp = 5;</code>
       */
      public boolean hasTimestamp() {
        return ((bitField0_ & 0x00000010) == 0x00000010);
      }
      /**
       * <code>required sfixed32 timestamp = 5;</code>
       */
      public int getTimestamp() {
        return timestamp_;
      }
      /**
       * <code>required sfixed32 timestamp = 5;</code>
       */
      public Builder setTimestamp(int value) {
        bitField0_ |= 0x00000010;
        timestamp_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>required sfixed32 timestamp = 5;</code>
       */
      public Builder clearTimestamp() {
        bitField0_ = (bitField0_ & ~0x00000010);
        timestamp_ = 0;
        onChanged();
        return this;
      }

      // @@protoc_insertion_point(builder_scope:com.sliang.cc3200.UdpCmd)
    }

    static {
      defaultInstance = new UdpCmd(true);
      defaultInstance.initFields();
    }

    // @@protoc_insertion_point(class_scope:com.sliang.cc3200.UdpCmd)
  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_sliang_cc3200_UdpCmd_descriptor;
  private static
    com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internal_static_com_sliang_cc3200_UdpCmd_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\rudp_cmd.proto\022\021com.sliang.cc3200\"]\n\006Ud" +
      "pCmd\022\016\n\006x_Left\030\001 \002(\017\022\017\n\007x_Right\030\002 \002(\017\022\016\n" +
      "\006y_Left\030\003 \002(\017\022\017\n\007y_Right\030\004 \002(\017\022\021\n\ttimest" +
      "amp\030\005 \002(\017B\013B\tPacketUdp"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
    internal_static_com_sliang_cc3200_UdpCmd_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_com_sliang_cc3200_UdpCmd_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessage.FieldAccessorTable(
        internal_static_com_sliang_cc3200_UdpCmd_descriptor,
        new java.lang.String[] { "XLeft", "XRight", "YLeft", "YRight", "Timestamp", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
