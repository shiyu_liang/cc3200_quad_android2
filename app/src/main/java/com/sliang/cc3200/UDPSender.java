package com.sliang.cc3200;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import com.sliang.cc3200.PacketUdp.UdpCmd;

/**
 * Created by liangste on 6/14/2015.
 */
public class UDPSender extends HandlerThread {
    private static final String TAG = "UDPSender";
    private static final String APIPString = "192.168.1.1";
    private static final int APPort = 5000;
    private Handler mHandler;
    private DatagramSocket mDatagramSocket;
    private DatagramPacket mDatagramPacket;
    InetAddress mInetAddress;

    public UDPSender(String name) {
        super(name);
    }

    @Override
    protected void onLooperPrepared() {
        super.onLooperPrepared();

        mHandler = new Handler(getLooper()) {
            @Override
            public void handleMessage(Message msg) {
                Log.i(TAG, "handleMessage " + msg.what);
                UdpCmd udpCmd = (UdpCmd) msg.obj;
                Log.i(TAG, "xLeft = " + udpCmd.getXLeft()/2.5 + " xRight = " + udpCmd.getXRight()/2.5);
                Log.i(TAG, "yLeft = " + udpCmd.getYLeft()/2.5 + " yRight = " + udpCmd.getYRight()/2.5);
                try {
                    byte[] cmdMsg = udpCmd.toByteArray();
                    mInetAddress = InetAddress.getByName(APIPString);
                    mDatagramSocket = new DatagramSocket();
                    mDatagramPacket = new DatagramPacket(cmdMsg,
                            cmdMsg.length,
                            mInetAddress,
                            APPort);
                    mDatagramSocket.send(mDatagramPacket);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (mDatagramSocket != null) {
                        mDatagramSocket.close();
                    }
                }
            };
        };
    }

    public void send(int XLeft, int XRight, int YLeft, int YRight, int TimeStamp) {
        UdpCmd udpCmd = UdpCmd.newBuilder()
                .setXLeft(XLeft)
                .setXRight(XRight)
                .setYLeft(YLeft)
                .setYRight(YRight)
                .setTimestamp(TimeStamp)
                .build();
        Message msg = new Message();
        msg.obj = udpCmd;
        mHandler.sendMessage(msg);
    }
}